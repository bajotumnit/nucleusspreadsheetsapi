# Nucleus Submissions -> Google Sheets

This project was created to facilitate automated Nucleus Submissions import into Google Spreadsheets.

The internal JSON API for checking submissions is used to get a list of all submissions for a particular card and is then converted to be usable in a spreadsheet.

The API is sufficient for grabbing submissions on a schedule and optionally sending emails.

The TypeScript conversion and general code quality and extendability upgrade was started on February 18th 2019.

## Submission structure
The API includes the following TS interfaces to shape submission structure, but during the collection of data many of the extraneous fields are discarded, and these interfaces are due to be refactored.
```Typescript
interface Submission extends ObjectConstructor {
    id: number;
    value: SubmissionField[];
    page_id: number;
    viewed: 0,
    old_id: number,
    deleted_at: string,
    created_at: string,
    updated_at: string,
    title: string,
    created_at_formatted: string,
    updated_at_formatted: string,
    created_at_formatted_full: string,
    updated_at_formatted_full: string
    [propName: string]: any
  }
  interface SubmissionField extends ObjectConstructor {
    layout?: string;
    label?: string;
    required?: boolean;
    value: string;
    mailchimpField?: string;
    planningCenterField?: string;
    [propName:string]: any
  }
  ```