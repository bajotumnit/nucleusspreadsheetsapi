declare namespace NucleusAPI {
  interface Submission extends ObjectConstructor {
    id: number;
    value: SubmissionField[];
    page_id: number;
    viewed: 0,
    old_id: number,
    deleted_at: string,
    created_at: string,
    updated_at: string,
    title: string,
    created_at_formatted: string,
    updated_at_formatted: string,
    created_at_formatted_full: string,
    updated_at_formatted_full: string
    [propName: string]: any
  }
  interface SubmissionField extends ObjectConstructor {
    layout?: string;
    label?: string;
    required?: boolean;
    value: string;
    mailchimpField?: string;
    planningCenterField?: string;
    [propName:string]: any
  }
  interface UpdatedCallback{
    (newSubmissions: Submission[]):void
  }
}
class NucleusAPI {
  private API_URL: string = "https://nucleus.church/api/submissions";
  private API_TOKEN: string = null;
  private googleDocID: string = null;
  private spreadsheetDocument: GoogleAppsScript.Spreadsheet.Spreadsheet = null;
  private spreadsheetSheet: GoogleAppsScript.Spreadsheet.Sheet = null;
  private nucleusPageID: number = null;
  private resizeColumnsToFit: boolean = true;
  private firstDataRow: number = 2;

  onComplete: (CallableFunction) = null;
  onUpdate: NucleusAPI.UpdatedCallback = null;

  constructor(
    nucleusPageID: number,
    apiToken: string,
    documentID: string,
    spreadsheetName: string = "Sheet1",
    firstDataRow: number = 2
  ) {
    this.firstDataRow = firstDataRow - 1; //2 by default...to allow for headers
    this.googleDocID = documentID;
    this.nucleusPageID = nucleusPageID;
    this.API_TOKEN = apiToken;
    this.spreadsheetDocument = SpreadsheetApp.openById(this.googleDocID);
    this.spreadsheetSheet = this.spreadsheetDocument.getSheetByName(
      spreadsheetName
    );
  }

  public fetch(): void {
    Logger.clear();
    const updatedCells = this.getJSON(
      `${this.API_URL}/${this.nucleusPageID}?api_token=${this.API_TOKEN}`
    );
    if (updatedCells && this.onUpdate) {
      this.onUpdate(updatedCells);
    }
    if (this.onComplete) {
      this.onComplete();
    }
  }

  public getDoc(): GoogleAppsScript.Spreadsheet.Spreadsheet {
    return this.spreadsheetDocument;
  }

  public setResize(autoResize: boolean): void {
    this.resizeColumnsToFit = autoResize == true;
  }

  /***
  getJSON() fetches the JSON from Nucleus, inserts new data into the specified spreadsheet and returns the entire JSON object for that new data for use in onUpdate.
  ***/
  private getJSON(aUrl: string): NucleusAPI.Submission[] {
    Logger.log(
      "Fetching values from Nucleus API...PageID = " + this.nucleusPageID
    );
    const response = UrlFetchApp.fetch(aUrl).getContentText(); // get feed
    if (response && response != "empty" && response != "") {
      const rawData = JSON.parse(response); //create json object
      this.createHeaders(rawData[0]);

      /**
      Currently only checking submission counts against the amount of rows currently in spreadsheet.
      This method works fine in most cases, but plan for future logic to check individual submissions against spreadsheet rows to allow for manual entries directly into spreadsheet.
      **/
      const oldDataLength = Math.max(
        0,
        this.getFirstEmptyRowByColumnArray() - this.firstDataRow - 1
      ); //Account for vertical offset. Minimum value of 0
      if (oldDataLength < rawData.length) {
        let data = this.cleanupSubmissions(rawData).slice(oldDataLength); //Only new data
        this.insertData(data);
        return data;
      }
    }
    return null;
  }

  /***
  createHeaders() takes a single entry array and generates headers on the spreadsheet at A1 if they do not already exist.
  The submissions JSON array for submissions contains the label from the Nucleus form.
  In theory these shouldn't change in a dataset, unless the form labels have been changes after submissions have been made.
  ***/
  private createHeaders(data: NucleusAPI.Submission): void {
    if (this.headerRowIsEmpty()) {
      let values = [
        data.value.map(col => {
          return col.label;
        })
      ];
      values[0].push("Created At");
      Logger.log("Generating headers:" + values);

      const headers = this.spreadsheetSheet.getRange(1, 1, 1, values[0].length);
      headers.setValues(values);
      headers.setFontWeight("bold");
      headers.setFontSize(14);
      headers.setFontFamily("Inconsolata");
    }
  }

  /***
  cleanupSubmissions() takes the JSON array and reformats it, adding the time object onto the value array within.
  It then pushes ONLY the value array, all other data contained on each submission is not required for our use.
  After the array has been reversed to keep new submissions on the bottom it is returned.
  ***/
  private cleanupSubmissions(submissions: NucleusAPI.Submission[]): NucleusAPI.Submission[] {
    const retData = submissions.map(s => {
      return (<NucleusAPI.Submission>Object).create({ ...s.value, created_at: s.created_at });
    });
    retData.reverse(); //Reverse data array so the oldest is on top
    return retData;
  }

  private insertData(objects: NucleusAPI.Submission[]): void {
    if (objects.length > 0) {
      Logger.log("Setting data for " + objects.length + " new rows");
      const dataColumnCount = objects[0].length; //How many columns of data are there? This should never change in the same dataset
      const insertData = objects.map((submission: any) =>
        submission.map((field: NucleusAPI.SubmissionField) => field.value)
      );
      Logger.log(insertData);
      const destinationRange = this.spreadsheetSheet.getRange(
        this.getFirstEmptyRowByColumnArray(),
        1,
        objects.length,
        dataColumnCount
      );
      destinationRange.setValues(insertData);
      destinationRange.setFontSize(10);
      destinationRange.setFontFamily("Inconsolata");
      if (this.resizeColumnsToFit) {
        for (let r = 1; r <= dataColumnCount; r++) {
          this.spreadsheetSheet.autoResizeColumn(r);
        }
      }
    }
  }
  //TODO:
  //More logic here to check actual submissions against spreadsheet.
  //Maybe there's a method for this in the Spreadsheets API?
  private isNewData(newDataLength: number): boolean {
    const oldDataLength = this.getFirstEmptyRowByColumnArray() - 1; //Sheets are count indexed. We need array index
    return oldDataLength < newDataLength;
  }

  // Don's array approach - checks first column only
  // With added stopping condition & correct result.
  // From answer https://stackoverflow.com/a/9102463/1677912
  private getFirstEmptyRowByColumnArray(): number {
    const column = this.spreadsheetSheet.getRange("A:A");
    const values = column.getValues(); // get all data in one call
    let ct = this.firstDataRow;
    while (values[ct] && values[ct][0] != "") {
      ct++;
    }
    return ++ct;
  }

  /***
  Similar to getFirstEmptyRowByColumnArray(), but only checks the first ROW, not COLUMN.
  ***/
  private headerRowIsEmpty(): boolean {
    const row = this.spreadsheetSheet.getRange("1:1");
    const values = row.getValues();
    let ct = 0;
    while (values[ct] && values[ct][0] != "") {
      ct++;
    }
    return ct == 0;
  }

  /***
  Sends email notifications with spreadsheet link and automated notification note, optionally includes debug logs
  ***/
  public sendEmail(
    to: string,
    subject: string,
    content: string,
    sendDebugLogs: boolean
  ): void {
    const body =
      (content !== undefined ? content + "\n\n\n" : "") +
      "Check out your spreadsheet at: https://drive.google.com/open?id=" +
      this.googleDocID +
      "\n\n" +
      "This is an automated notification. If you would like to be removed from this notification list, please reply to this message." +
      (sendDebugLogs ? "\n\n" + Logger.getLog() : "");
    GmailApp.sendEmail(to, subject, body, {
      from: "notify@mosaicabq.church",
      name: "MosaicABQ.church",
      bcc: "webmaster@mosaicabq.com"
    });
  }
}
